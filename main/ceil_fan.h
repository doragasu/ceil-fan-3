#ifndef __CEIL_FAN_H__
#define __CEIL_FAN_H__

#include <stdint.h>
#include <stdbool.h>

enum cf_light {
	CF_LIGHT_OFF = 0,
	CF_LIGHT_ON,
	__CF_LIGHT_MAX
};

enum cf_fan {
	CF_FAN_OFF = 0,
	CF_FAN_1,
	CF_FAN_2,
	CF_FAN_3,
	__CF_FAN_MAX
};

struct cf_config {
	enum cf_light light_initial_state;
	enum cf_fan fan_initial_state;
	bool polarity;
	uint8_t gpio_light;
	uint8_t gpio_fan1;
	uint8_t gpio_fan2;
	uint8_t gpio_fan3;
	uint8_t gpio_light_toggle;
	uint8_t gpio_fan_toggle;
};

int cf_init(const struct cf_config *config);
void cf_light_set(enum cf_light light);
enum cf_light cf_light_get(void);
void cf_light_toggle(void);

void cf_fan_set(enum cf_fan fan);
enum cf_fan cf_fan_get(void);
void cf_fan_toggle(void);

#endif
