#include <driver/gpio.h>
#include <esp_attr.h>
#include <esp_timer.h>

#include "ceil_fan.h"

#define OUTPUT_SET(dev, on) gpio_set_level(cfg.gpio_ ## dev, (on) ? \
		(cfg.polarity) : !(cfg.polarity));

// 500 ms debounce time
#define DEBOUNCE_US	500000

static struct cf_config cfg = {};
static enum cf_light light = CF_LIGHT_OFF;
static enum cf_fan fan = CF_FAN_OFF;

static IRAM_ATTR void light_toggle_cb(__attribute__((unused)) void *arg)
{
	static uint64_t last = 0;
	uint64_t current = esp_timer_get_time();

	if ((current - last) > DEBOUNCE_US) {
		cf_light_toggle();
		last = current;
	}
}

static IRAM_ATTR void fan_toggle_cb(__attribute__((unused)) void *arg)
{
	static uint64_t last = 0;
	uint64_t current = esp_timer_get_time();

	if ((current - last) > DEBOUNCE_US) {
		cf_fan_toggle();
		last = current;
	}
}

int cf_init(const struct cf_config *config)
{
	cfg = *config;
	const gpio_config_t in_cfg = {
		.mode = GPIO_MODE_INPUT,
		.pull_up_en = GPIO_PULLUP_ENABLE,
		.pin_bit_mask = BIT(cfg.gpio_light_toggle) |
			BIT(cfg.gpio_fan_toggle),
		.intr_type = GPIO_INTR_ANYEDGE
	};
	const gpio_config_t out_cfg = {
		.mode = GPIO_MODE_OUTPUT,
		.pin_bit_mask = BIT(cfg.gpio_fan1) |
			BIT(cfg.gpio_fan2) |
			BIT(cfg.gpio_fan3) |
			BIT(cfg.gpio_light)
	};

	light = cfg.light_initial_state;
	cf_light_set(light);
	fan = cfg.fan_initial_state;
	cf_fan_set(fan);
	gpio_config(&out_cfg);
	gpio_config(&in_cfg);
	//install gpio isr service
	gpio_install_isr_service(0);
	//hook isr handler for specific gpio pin
	gpio_isr_handler_add(cfg.gpio_light_toggle, light_toggle_cb,
			(void*)cfg.gpio_light_toggle);
	gpio_isr_handler_add(cfg.gpio_fan_toggle, fan_toggle_cb,
			(void*)cfg.gpio_fan_toggle);

	return 0;
}

void cf_light_set(enum cf_light _light)
{
	bool out;
	light = _light;

	switch (light) {
	case CF_LIGHT_ON:
		out = 1;
		break;
	default:
		light = CF_LIGHT_OFF;
		// Fallthrough
	case CF_LIGHT_OFF:
		out = 0;
	}

	OUTPUT_SET(light, out);
}

void cf_fan_set(enum cf_fan _fan)
{
	bool out1, out2, out3;
	fan = _fan;
	switch (fan) {
	case CF_FAN_1:
		out1 = 0;
		out2 = out3 = 1;
		break;

	case CF_FAN_2:
		out2 = 0;
		out1 = out3 = 1;
		break;

	case CF_FAN_3:
		out3 = 0;
		out1 = out2 = 1;
		break;

	default:
		fan = CF_FAN_OFF;
		// Fallthrough
	case CF_FAN_OFF:
		out1 = out2 = out3 = 0;
	}

	OUTPUT_SET(fan1, out1);
	OUTPUT_SET(fan2, out2);
	OUTPUT_SET(fan3, out3);
}

enum cf_light cf_light_get(void)
{
	return light;
}

enum cf_fan cf_fan_get(void)
{
	return fan;
}

void cf_light_toggle(void)
{
	cf_light_set(light ? CF_LIGHT_OFF : CF_LIGHT_ON);
}

void cf_fan_toggle(void)
{
	cf_fan_set(fan + 1);
}
