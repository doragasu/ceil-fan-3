#ifndef __TELEGRAM_H__
#define __TELEGRAM_H__

// Not the best place to put this
#define FLCTRL_VERSION "0.4"

// No events supported yet, so this enum and following union are placeholders
enum tg_event_type {
	__TG_EVENT_MAX
};

union tg_data {
};

typedef void (*tg_event_cb)(enum tg_event_type event, union tg_data *data);

int tg_init(void);
void tg_deinit(void);
int tg_parser_start(const char *hello_msg, tg_event_cb event_cb);
void tg_parser_stop(void);

#endif /*__TELEGRAM_H__*/
