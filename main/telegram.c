#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_wifi.h>

#include <bate.h>

#include "ceil_fan.h"
#include "util.h"
#include "telegram.h"
#include "upgrade.h"
#include "reset.h"

// Task is low priority
#define TG_TASK_PRIO (tskIDLE_PRIORITY + 1)
#define TG_TASK_STACK 4096
#define TG_CHAT_ID_MAX_LEN 31

#define TG_ERROR_MAX 5

static struct {
	tg_event_cb event_cb;
	bool parser_enable;
} tg = {};

#define COMMAND_TABLE(X_MACRO) \
	X_MACRO(START, start, /start) \
	X_MACRO(LIGHT, light, Light) \
	X_MACRO(STATUS, status, Status) \
	X_MACRO(UPGRADE, upgrade, Upgrade) \
	X_MACRO(MEMORY, memory, Memory) \
	X_MACRO(FAN_OFF, fan_off, Fan_off) \
	X_MACRO(FAN_1, fan_1, Fan_1) \
	X_MACRO(FAN_2, fan_2, Fan_2) \
	X_MACRO(FAN_3, fan_3, Fan_3) \

#define X_AS_CMD_ENUM(uname, lname, str) CMD_ ## uname,
enum cmd {
	COMMAND_TABLE(X_AS_CMD_ENUM)
	__CMD_MAX
};

#define X_AS_CMD_STRINGS(uname, lname, str) #str,
static const char * const keyb[__CMD_MAX + 1] = {
	COMMAND_TABLE(X_AS_CMD_STRINGS)
	NULL
};
static const unsigned int cols[] = {4, 4};

#define X_AS_CMD_PARSE_FN_PROTO(uname, lname, str) \
	static void cmd_ ## lname ## _parse(const char*, \
		const struct bate_message*, \
		const struct bate_chat*);
COMMAND_TABLE(X_AS_CMD_PARSE_FN_PROTO);

#define X_AS_CMD_PARSE_FN(uname, lname, str) cmd_ ## lname ## _parse,
static void (*message_parse_fn[__CMD_MAX])(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat) = {
	COMMAND_TABLE(X_AS_CMD_PARSE_FN)
};

static struct bate_bot_info *bot_get(void)
{
	cJSON *json = NULL;
	struct bate_bot_info *bot = NULL;

	bate_me_get(&json);
	bot = bate_me_parse(json);
	cJSON_Delete(json);

	return bot;
}

static void hello_msg(const char *chat_id)
{
	const char * const text = "Fan light control version " FLCTRL_VERSION
		".\nLook above! you can toggle 🔘 with this bot!";

	bate_keyboard_reply(chat_id, text, BATE_TEXT_MARKDOWN,
			(const char**)&keyb[1], ARRAY_SIZE(cols), cols,
			BATE_KEYB_RESIZE, NULL);
}

static void cmd_start_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	UNUSED_PARAM(chat);
	UNUSED_PARAM(msg);
	hello_msg(chat_id);
}

static void cmd_status_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	UNUSED_PARAM(msg);
	UNUSED_PARAM(chat);
	const char * const emoji[2] = {"💤", "💡"};
	const char * const stat[2] = {"OFF", "ON"};
	const char * const fan_emo[4] = {"🔥", "🌬", "❄️", "🥶"};
	char line[64];

	const enum cf_light light = cf_light_get();
	const enum cf_fan fan = cf_fan_get();
	snprintf(line, 64, "%s Light is _%s_, %s Fan speed is _%d_",
			emoji[light], stat[light], fan_emo[fan], fan);
	bate_msg_send(chat_id, line, BATE_TEXT_MARKDOWN, NULL);
}

static void cmd_light_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	cf_light_toggle();
	cmd_status_parse(chat_id, msg, chat);
}

static void cmd_memory_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	char report[64];
	UNUSED_PARAM(msg);
	UNUSED_PARAM(chat);

	const uint32_t mem_free = esp_get_free_heap_size();
	const uint32_t mem_min = esp_get_minimum_free_heap_size();
	snprintf(report, sizeof(report), "Free RAM: %"PRIu32", min: %"PRIu32,
			mem_free, mem_min);
	bate_msg_send(chat_id, report, BATE_TEXT_MARKDOWN, NULL);
}

static void cmd_upgrade_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	UNUSED_PARAM(msg);
	UNUSED_PARAM(chat);
	int err;

	bate_msg_send(chat_id, "⏰ Starting upgrade, please wait...",
			BATE_TEXT_PLAIN, NULL);

	err = upgrade_firmware(CONFIG_FIRMWARE_UPGRADE_SERVER, "upgrade.bin");
	if (!err) {
		bate_msg_send(chat_id, "👍 Upgrade succeeded, firmware will "
				"restart in 5 seconds.",
				BATE_TEXT_PLAIN, NULL);
		vTaskDelay(pdMS_TO_TICKS(5000));
		tg.parser_enable = false;
	} else {
		bate_msg_send(chat_id, "😪 Upgrade failed, please try again "
				"later.", BATE_TEXT_PLAIN, NULL);
	}
}

static void cmd_fan_off_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	cf_fan_set(CF_FAN_OFF);
	cmd_status_parse(chat_id, msg, chat);
}

static void cmd_fan_1_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	cf_fan_set(CF_FAN_1);
	cmd_status_parse(chat_id, msg, chat);
}

static void cmd_fan_2_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	cf_fan_set(CF_FAN_2);
	cmd_status_parse(chat_id, msg, chat);
}

static void cmd_fan_3_parse(const char *chat_id,
		const struct bate_message *msg,
		const struct bate_chat *chat)
{
	cf_fan_set(CF_FAN_3);
	cmd_status_parse(chat_id, msg, chat);
}

static void unsupported_command(const char *chat_id,
		const struct bate_chat *chat)
{
	char msg[128];

	snprintf(msg, 128, "Sorry _%s_, I do not understand that!",
			chat->username);
	bate_msg_send(chat_id, msg, BATE_TEXT_MARKDOWN, NULL);
}

static void message_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	int command;

	if (msg && msg->text) {
		command = token_index((const char**)keyb, msg->text);
		if (command >= 0) {
			message_parse_fn[command](chat_id, msg, chat);
		} else {
			unsupported_command(chat_id, chat);
		}
	} else {
		LOGW("Ignoring message with no text");
	}
}

static bool is_user_allowed(const char *user_id)
{
	const char *pos = CONFIG_TELEGRAM_ALLOWED_IDS;
	int len;

	while (*pos) {
		for (len = 0; pos[len] && pos[len] != ','; len++);
		if (0 == memcmp(user_id, pos, len)) {
			return true;
		}
		pos += len;

		if (',' == *pos) {
			pos++;
		}
	}

	return false;
}

static void message_json_parse(const cJSON *msg_json)
{
	char chat_id[32];
	struct bate_message *msg = bate_message_parse(msg_json);
	struct bate_chat *chat = bate_chat_parse(msg->chat);

	snprintf(chat_id, 32, "%ld", chat->id);
	if (is_user_allowed(chat_id)) {
		// Message from the client allowed to use the bot.
		// Messages from non-authorized users are silently discarded.
		message_parse(chat_id, msg, chat);
	}

	bate_message_free(msg);
	bate_chat_free(chat);
}

static long process_update(cJSON *update)
{
	long next;
	struct bate_update *up = bate_update_parse(update);
	LOGI("update_id: %ld", up->update_id);

	switch (up->type) {
	case BATE_UPDATE_TYPE_MESSAGE:
		message_json_parse(up->message);
		break;

	default:
		LOGD("got unsupported update %d", up->type);
		break;
	}


	next = up->update_id + 1;
	bate_update_free(up);

	return next;
}

// Restart preserving light status on RTC memory
static void deep_sleep_restart(void)
{
	esp_wifi_stop();
	const enum cf_light light = cf_light_get();
	const enum cf_fan fan = cf_fan_get();
	rst_rtc_mem_write(&light, 0, 1);
	rst_rtc_mem_write(&fan, 1, 1);
	LOGI("entering deep sleep with light: %d, fan: %d", light, fan);
	rst_deepsleep();
}

void telegram_tsk(void *arg)
{
	char *hello_msg = arg;

	struct bate_bot_info *bot = NULL;
	long next_update = 0;
	cJSON *root, *updates, *update;
	uint32_t errors = 0;

	LOGI("Telegram bot start");
	while (!(bot = bot_get())) {
		vTaskDelay(pdMS_TO_TICKS(30000));
	}

	LOGI("connected: id = %ld, first_name = %s, username = %s", bot->id,
			bot->first_name, bot->username);

	if (hello_msg) {
		LOGD("Sending hello message");
		bate_msg_send(CONFIG_TELEGRAM_REPORT_ID, hello_msg,
				BATE_TEXT_MARKDOWN, NULL);
		free(hello_msg);
	}

	while (tg.parser_enable && errors < TG_ERROR_MAX) {
		if (200 == bate_updates_get(next_update, 30, &root)) {
			updates = json_get_array(root, "result");
			cJSON_ArrayForEach(update, updates) {
				next_update = process_update(update);
			}
			cJSON_Delete(root);
			errors = 0;
		} else {
			errors++;
		}
	}
	if (!tg.parser_enable) {
		// Make a final request to mark the last message as read
		bate_updates_get(next_update, 0, NULL);
		LOGI("Telegram bot end due to user request");
	} else {
		// Exited due to errors, reboot
		LOGE("Telegram bot ended unexpectedly");
	}

	bate_me_free(bot);
	deep_sleep_restart();
}

int tg_init(void)
{
	// Nothing to do
	return 0;
}

void tg_deinit(void)
{
	// TODO: stop task if running
}

int tg_parser_start(const char *hello_msg, tg_event_cb event_cb)
{
	BaseType_t result;

	tg.event_cb = event_cb;
	tg.parser_enable = true;
	char *msg = NULL;

	if (hello_msg) {
		msg = strdup(hello_msg);
	}

	result = xTaskCreate(telegram_tsk, "telegram", TG_TASK_STACK,
			(void*)msg, TG_TASK_PRIO, NULL);

	return result != pdPASS;
}

void tg_parser_stop(void)
{
	tg.parser_enable = false;
}
