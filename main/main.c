#include <nvs_flash.h>
#include <string.h>

#include "esp_system.h"
#include "wifi_sta.h"
#include "ceil_fan.h"
#include "reset.h"
#include "telegram.h"
#include "util.h"

// WARNING: D0 must be externally wired to RST for the deep sleep reset to work.

// Light toggle sense pin
#define LIGHT_TOGGLE_PIN 4 // D2 on NodeMCU
// Relay control output pin
#define LIGHT_OUT_PIN 5    // D1 on NodeMCU

static bool tg_started = false;
static esp_reset_reason_t rst_reason;

void wifi_ev_cb(enum wifi_event event, union wifi_event_data *data)
{
	UNUSED_PARAM(data);
	char buf[64];

	if (WIFI_EVENT_GOT_IP == event && !tg_started) {
		char *msg = NULL;
		if (ESP_RST_POWERON != rst_reason && ESP_RST_EXT != rst_reason) {
			snprintf(buf, sizeof(buf), "WARNING: RESET DUE TO: %s",
					rst_reason_str_get(rst_reason));
			msg = buf;
		}
		tg_started = !tg_parser_start(msg, NULL);
	}
}

void app_main(void)
{
	enum cf_light light = CF_LIGHT_ON;
	enum cf_fan fan = CF_FAN_OFF;

	rst_reason = rst_reason_get();
	// On power-on reset, we keep the light ON by default. Else try reading
	// the value from the RTC memory. As the RTC memory only preserves its
	// value when exiting from deep sleep, on all other events (software
	// reset, watchdog reset, etc.) the ligth will be turned off.
	if (ESP_RST_POWERON == rst_reason || ESP_RST_EXT == rst_reason) {
		LOGI("ligth ON after POWER or EXT reset");
	} else if (ESP_RST_DEEPSLEEP == rst_reason) {
		rst_rtc_mem_read(&light, 0, 1);
		rst_rtc_mem_read(&fan, 1, 1);
		LOGI("restoring status, light: %d, fan: %d", light, fan);
	} else {
		light = CF_LIGHT_OFF;
		LOGI("unexpected reset reason, setting light OFF");
	}

	const struct cf_config fan_cfg = {
		.light_initial_state = light,
		.fan_initial_state = fan,
		.polarity = 0,          // Relays turn on with '0' logic level
		.gpio_light = 2,        // D4
		.gpio_fan1 = 14,        // D5
		.gpio_fan2 = 12,        // D6
		.gpio_fan3 = 13,        // D7
		.gpio_light_toggle = 5, // D1
		.gpio_fan_toggle = 4    // D2
	};

	// Configure light module early, for the light to be turned on
	// as soon as possible when device is powered
	cf_init(&fan_cfg);
	LOGI("STARTING FAN CONTROL VERSION " FLCTRL_VERSION);

	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	wifi_init_sta(wifi_ev_cb);
	ESP_ERROR_CHECK(tg_init());
}
